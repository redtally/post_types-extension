<?php namespace Redtally\PostTypesExtension;

use Anomaly\PostsModule\Type\Contract\TypeInterface;
use Anomaly\PostsModule\Type\TypeModel;
use Anomaly\PostsModule\Type\TypeRepository;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Ui\Table\Event\TableIsQuerying;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Redtally\PostTypesExtension\Command\AttachUrlMethodToPost;
use Redtally\PostTypesExtension\Listener\AddTypeFilterAndColumn;

/**
 * Class PostTypesExtensionServiceProvider
 * @package Redtally\PostTypesExtension
 */
class PostTypesExtensionServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        "posts/{slug}"                        => [
            'as'   => 'redtally.extension.post_types::posts.override',
            'uses' => 'Redtally\PostTypesExtension\Http\Controller\PostTypesController@override',
        ],
        "post/{slug}"                        => [
            'as'   => 'redtally.extension.post_types::posts.view_default',
            'uses' => 'Anomaly\PostsModule\Http\Controller\PostsController@view',
        ]
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Redtally\PostTypesExtension\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * The addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        TableIsQuerying::class => [
            AddTypeFilterAndColumn::class
        ]
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Redtally\PostTypesExtension\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        dispatch(new AttachUrlMethodToPost());
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
        if (request()->segment(1) == 'admin')
            return;

        /* @var TypeInterface $postType */
        foreach (TypeModel::all() as $postType) {
            $router->get($postType->getSlug().'/{slug}', 'Redtally\PostTypesExtension\Http\Controller\PostTypesController@view');
        }
    }

}
