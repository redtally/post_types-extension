<?php namespace Redtally\PostTypesExtension\Command;

use Anomaly\PostsModule\Post\PostModel;
use Anomaly\Streams\Platform\Traits\Hookable;

/**
 * Class AttachUrlMethodToPost
 * @package Redtally\PostTypesExtension\Command
 */
class AttachUrlMethodToPost
{
    /**
     * Attach a getUrl() method to post which takes into account default etc.
     */
    public function handle()
    {
        /** @var PostModel $post */
        $post = new PostModel();

        /* @var Hookable $post */
        if (!method_exists($post, 'bind')) {
            return;
        }

        /**
         * Attach method to retrieve post URL.
         */
        $post->bind(
            'get_url',
            function () {
                return url(($this->getTypeSlug() !== 'default' ? $this->getTypeSlug() : 'post') . '/' . $this->getSlug());
            }
        );
    }
}