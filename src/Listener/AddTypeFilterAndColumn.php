<?php namespace Redtally\PostTypesExtension\Listener;

use Anomaly\PostsModule\Post\Table\PostTableBuilder;
use Anomaly\PostsModule\Type\TypeModel;
use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeBuilder;
use Anomaly\Streams\Platform\Support\Evaluator;
use Anomaly\Streams\Platform\Support\Resolver;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Type\SelectFilter;
use Anomaly\Streams\Platform\Ui\Table\Component\Header\Header;
use Anomaly\Streams\Platform\Ui\Table\Event\TableIsQuerying;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AddTypeFilterAndColumn
 * @package Redtally\PostTypesExtension\Listener
 */
class AddTypeFilterAndColumn
{
    /**
     * @param TableIsQuerying $event
     */
    public function handle(TableIsQuerying $event)
    {
        $builder = $event->getBuilder();
        $query = $event->getQuery();

        if (get_class($builder) == PostTableBuilder::class) {
            $this->addTypeFilter($builder);
            $this->addTypeColumn($builder);
            $this->getFilteredQuery($query);
        }
    }

    /**
     * Add the post type column to the table.
     *
     * @param PostTableBuilder $builder
     */
    protected function addTypeColumn(PostTableBuilder $builder)
    {
        $builder->addColumn([
            'value' => function ($entry) {
                return $entry->getTypeSlug();
            }
        ]);

        $header = new Header();
        $header->setSortColumn('type_id');
        $header->setSortable(true);
        $header->setBuilder($builder);
        $header->setHeading('redtally.extension.post_types::field.post_type.name');

        $builder->getTable()->addHeader($header);
    }

    /**
     * Add a filter for post type.
     *
     * @param PostTableBuilder $builder
     */
    protected function addTypeFilter(PostTableBuilder $builder)
    {
        $filter = new SelectFilter(app(FieldTypeBuilder::class), app(Resolver::class), app(Evaluator::class));
        $filter->setPlaceholder('redtally.extension.post_types::field.post_type.name');
        $filter->setOptions($this->getOptions());
        $filter->setSlug('type');

        $builder->getTable()->addFilter($filter);
    }

    /**
     * Filter by post type if present in request.
     *
     * @param Builder $query
     * @return Builder
     */
    protected function getFilteredQuery(Builder $query)
    {
        if ($filterType = request('filter_type')) {
            $query->where('type_id', '=', $filterType);
        }

        return $query;
    }

    /**
     * Get post type options for select.
     *
     * @return array
     */
    private function getOptions()
    {
        $typeModels = TypeModel::all();

        $options = [];

        foreach ($typeModels as $typeModel) {
            $options[$typeModel->id] = $typeModel->getName();
        }

        return $options;
    }
}