<?php namespace Redtally\PostTypesExtension\Http\Controller;

use Anomaly\PostsModule\Post\Command\MakePostResponse;
use Anomaly\PostsModule\Post\PostRepository;
use Anomaly\PostsModule\Post\PostResolver;
use Anomaly\PostsModule\Type\TypeRepository;
use Anomaly\Streams\Platform\Http\Controller\PublicController;

/**
 * Class PostTypesController
 * @package Redtally\PostTypesExtension\Http\Controller
 */
class PostTypesController extends PublicController
{
    /**
     * @var TypeRepository
     */
    private $postTypes;

    /**
     * @var PostRepository
     */
    private $posts;

    /**
     * PostTypesController constructor.
     * @param TypeRepository $postTypes
     * @param PostRepository $posts
     */
    public function __construct(TypeRepository $postTypes, PostRepository $posts)
    {
        $this->postTypes = $postTypes;
        $this->posts = $posts;
        parent::__construct();
    }

    /**
     * @param $slug
     * @param PostResolver $resolver
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function view($slug, PostResolver $resolver)
    {
        $typeSlug = request()->segment(1);

        if (!$postType = $this->postTypes->findBySlug($typeSlug))
            abort(404);

        if (!$post = $resolver->resolve())
            abort(404);

        if ($typeSlug == 'default')
            return redirect()->to('post/'.$post->getSlug());

        if ($post->getTypeSlug() !== $typeSlug)
            return redirect()->to($post->getTypeSlug().'/'.$post->getSlug());

        if (!$post->isLive())
            abort(404);

        $this->dispatch(new MakePostResponse($post));

        return $post->getResponse();
    }

    /**
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function override($slug)
    {
        if (!$post = $this->posts->findBySlug($slug))
            abort(404);

        /** @var string $path This is attached to the Post model by the AttachUrlMethodToPost command. */
        $path = $post->getUrl();

        return redirect()->to($path);
    }
}